﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Options;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Context
{
    public class ApplicationDbContext: DbContext
    {
        private readonly IOptions<ConfigurationOptions> _configeration;
        
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options,
            IOptions<ConfigurationOptions> configeration) : base(options)
        {
            _configeration = configeration;
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                var stringConnection = _configeration.Value.ConnectionStrings.SqlLite;
                optionsBuilder.UseSqlite(stringConnection);
            }
            SQLitePCL.Batteries.Init();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PromoCode>()
                .HasOne(x => x.PartnerManager)
                .WithMany();
            
            modelBuilder.Entity<PromoCode>()
                .HasOne(x => x.Preference)
                .WithMany();
            
            modelBuilder.Entity<Employee>()
                .HasOne(x => x.Role)
                .WithMany();
            
            modelBuilder.Entity<Customer>()
                .HasMany(x => x.PromoCodes)
                .WithOne()
                .OnDelete(DeleteBehavior.Cascade);
            
            modelBuilder.Entity<CustomerPreference>()
                .HasKey(x => new {x.CustomerId, x.PreferenceId,});
            
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(x => x.Customer)
                .WithMany(x => x.CustomerPreferences)
                .HasForeignKey(x => x.CustomerId);
            
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(x => x.Preference)
                .WithMany()
                .HasForeignKey(x => x.PreferenceId);
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Partner> Partners { get; set; }
    }
}