﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Preference
{
    public class PreferenceResponse
    {
        public PreferenceResponse(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}