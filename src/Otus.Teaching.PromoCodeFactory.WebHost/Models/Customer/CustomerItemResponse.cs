﻿using System;
using CustomerModel = Otus.Teaching.PromoCodeFactory.Core.Domain.Models.Customer;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Customer
{
    public class CustomerItemResponse
    {
        public CustomerItemResponse()
        {
        }

        public CustomerItemResponse(Guid id, string fullName, string lastName, string email)
        {
            Id = id;
            FirstName = fullName;
            LastName = lastName; 
            Email = email;
        }

        public CustomerItemResponse(CustomerModel customer)
        {
            Id = customer.Id;
            FirstName = customer.FirstName;
            LastName = customer.LastName;
            Email = customer.Email;
        }

        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; } 
    }
}