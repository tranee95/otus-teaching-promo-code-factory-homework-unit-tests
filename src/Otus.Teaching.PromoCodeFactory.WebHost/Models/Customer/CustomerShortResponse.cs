﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Preference;
using CustomerModel = Otus.Teaching.PromoCodeFactory.Core.Domain.Models.Customer;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Customer
{
    public class CustomerShortResponse
    {
        public CustomerShortResponse()
        {
        }

        public CustomerShortResponse(CustomerModel customer)
        {
            Id = customer.Id;
            Email = customer.Email;
            FullName = customer.FullName;

            CustomerPreference = 
                customer.CustomerPreferences
                .Select(s => new PreferenceResponse(s.Preference.Name))
                .ToList();
        }

        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }

        public List<PreferenceResponse> CustomerPreference { get; set; }
    }
}