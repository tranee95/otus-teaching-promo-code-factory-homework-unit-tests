﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Role;
using EmployeeModel = Otus.Teaching.PromoCodeFactory.Core.Domain.Models.Employee;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Employee
{
    public class EmployeeResponse
    {
        public EmployeeResponse()
        {
        }

        public EmployeeResponse(EmployeeModel employee)
        {
            Id = employee.Id;
            Email = employee.Email;
            /*Role = employee.Role.Select(x => new RoleItemResponse()
            {
                Name = x.Name,
                Description = x.Description
            }).ToList();*/
            FullName = employee.FullName;
            AppliedPromocodesCount = employee.AppliedPromocodesCount;
        }

        public Guid Id { get; private set; }
        public string FullName { get; set; }

        public string Email { get; set; }

        public List<RoleItemResponse> Roles { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}