﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Employee
{
    public class EmployeeShortResponse
    {
        public EmployeeShortResponse()
        {
        }

        public EmployeeShortResponse(Guid id, string email, string fullName)
        {
            Id = id;
            Email = email;
            FullName = fullName;
        }

        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
    }
}