﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]/[action]")]
    public class PromoCodesController : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public PromoCodesController(
            IRepository<PromoCode> promoCodeRepository,
            IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        [HttpGet]
        public async Task<List<PromoCode>> GetPromoCodesAsync()
        {
            var result = await _promoCodeRepository.GetAllAsync();
            return result.ToList();
        }

        [HttpGet]
        public async Task<PromoCode> GetPromoCodeAsync(Guid id)
        {
            return await _promoCodeRepository.GetByIdAsync(id);
        }

        [HttpPost]
        public async Task<PromoCode> AddPromoCodeAsync(PromoCode promoCode)
        {
            promoCode.GeneratedId();
            await _promoCodeRepository.AddAsync(promoCode);

            return promoCode;
        }

        [HttpPost]
        public async Task<IActionResult> GivePromocodesToCustomersWithPreferenceAsync(
            string preferensName,
            PromoCode promoCode)
        {
            promoCode.GeneratedId();
            
            var preference = _preferenceRepository.Find(s => s.Name == preferensName);
            var customers = _customerRepository.Where(s =>
                s.CustomerPreferences.Any(x => x.Preference.Id == preference.Id));

            foreach (var customer in customers)
            {
                customer.PromoCodes.Add(promoCode);
                await _customerRepository.UpdateAsync(customer);
            }

            return Ok();
        }
    }
}