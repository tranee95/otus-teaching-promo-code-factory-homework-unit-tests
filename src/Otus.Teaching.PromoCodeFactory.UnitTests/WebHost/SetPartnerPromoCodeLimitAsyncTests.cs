﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Partner;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private SetPartnerPromoCodeLimitRequest  _setPartnerPromoCodeLimitRequest;
        
        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());

            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
            _setPartnerPromoCodeLimitRequest = new Mock<SetPartnerPromoCodeLimitRequest>().Object;
        }
        
        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };

            return partner;
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync((Partner)null);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, _setPartnerPromoCodeLimitRequest);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            partner.IsActive = false;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, _setPartnerPromoCodeLimitRequest);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimit_ReturnsBadRequest()
        {
            // Arrange 
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            var activeLimit = partner.PartnerLimits.ToList();

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, _setPartnerPromoCodeLimitRequest);

            // Assert
            Assert.Equal(0, partner.NumberIssuedPromoCodes);
            Assert.Equal(activeLimit[0].CancelDate?.ToString("yy-MMM-dd ddd"), DateTime.Now.ToString("yy-MMM-dd ddd"));
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimitZero_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            _setPartnerPromoCodeLimitRequest.Limit = -1;

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, _setPartnerPromoCodeLimitRequest);

            // Assert
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal("Лимит должен быть больше 0",badRequestResult.Value);

        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SaveLimitToBd_CreatedAtActionResult()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            _setPartnerPromoCodeLimitRequest.Limit = 1;

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, _setPartnerPromoCodeLimitRequest);

            // Assert
            var actionResult = Assert.IsType<CreatedAtActionResult>(result);
            
            var returnValue = Assert.IsType<RouteValueDictionary>(actionResult.RouteValues);
            Assert.Equal("id", returnValue.Keys.ElementAt(0));
            Assert.Equal("limitId", returnValue.Keys.ElementAt(1));
            Assert.Equal(Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"), returnValue.Values.ElementAt(0));
            Assert.Equal(Guid.Parse("00000000-0000-0000-0000-000000000000"), returnValue.Values.ElementAt(1));
        }
    }
}